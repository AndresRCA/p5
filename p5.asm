list P = 16F877A
include <P16F877A.inc>

org 0h
;*********** Configuracion ***************

;***************** PWM *******************
;1. Set the PWM period by writing to the PR2 register.
;2. Set the PWM duty cycle by writing to the CCPR1L register and CCP1CON<5:4> bits.
;3. Make the CCP1 pin an output by clearing the TRISC<2> bit.
;4. Set the TMR2 prescale value and enable Timer2 by writing to T2CON.
;5. Configure the CCP1 module for PWM operation.
;*****************************************

		movlw d'0' ; PWMduty cycle = (CCPR1L : CCP1CON(5,4))*Tosc*(Prescaler de TMR2)
		; donde Duty es el octeto mas significativo del numero completo, ejemplo: duty cycle de 25% seria
		; 0.25*PWMperiodo = X*Tosc*(Prescaler de TMR2)
		; En nuestro caso: duty cycle de 100% es decir, ADRESH = 255, hallo el periodo -> 1*PWMperiodo = 1020*(1/4*10^-6)*16 = 4.08*10^-3, 1020 es 11111111:00 => CCPR1L:CCP1CON(5,4) con CCPR1L = ADRESH
		movwf CCPR1L ; cargo Duty en CCPR1L, el resto de los bits de CCP1CON son 0 por defecto, en este caso el duty sera de 0 al inicio
		bsf STATUS, 5
		movlw d'254' ; PWMperiodo = (PR2+1)*4*Tosc*(Prescaler de TMR2) => PR2 = (4.08*10^-3)/(10^-6 * 16) = 254
		movwf PR2 ; el valor calculado de X se carga para el periodo de la onda, el valor de PR2, no se debe exceder de 255 o ser negativo en el calculo, si se excede se debe usar otro prescaler
		bcf TRISC, 2 ; configuro pin ccp1 como salida
        bcf STATUS, 5
		bsf T2CON, 1 ; configuro el prescaler 16 de TMR2, 00 = 1, 01 = 4, 1X = 16
		bsf T2CON, 2 ; prendo el TMR2
		bsf CCP1CON, 3 ; configuro el modulo CCP1 como PWM (11xx de bits 3-0)
		bsf CCP1CON, 2
		bsf ADCON0, 0 ; enciendo el modulo conversor
;*****************************************
Convert	bsf ADCON0, 2
Wait	btfsc ADCON0, 2
		goto Wait
		movf ADRESH, 0
		movwf CCPR1L
		goto Convert
;*************************************************************
end
